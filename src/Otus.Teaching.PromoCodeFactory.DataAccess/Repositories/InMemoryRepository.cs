﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T item)
        {
            item.Id = Guid.NewGuid();
            Data = Data.Append(item);
            return Task.FromResult(item.Id);
        }

        public Task UpdateAsync(T item)
        {
            var temp = Data.ToList();

            var index = temp.FindIndex(x => x.Id == item.Id);
            
            temp[index] = item;

            Data = temp;

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            var temp = Data.ToList();

            var index = temp.FindIndex(0, temp.Count, x => x.Id == id);

            temp.RemoveAt(index);

            Data = temp;

            return Task.CompletedTask;
        }
    }
}