﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeesController(
            IRepository<Employee> employeeRepository, 
            IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeShortResponse>>(employees);

            return employeesModelList.ToList();
        }

        /// <summary>
        /// Получить данные сотрудника по Id.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = _mapper.Map<EmployeeResponse>(employee);

            return employeeModel;
        }

        /// <summary>
        /// Добавить нового сотрудника.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync([FromBody] EmployeeCreateRequest request)
        {
            var employee = _mapper.Map<Employee>(request);

            var employeeId = await _employeeRepository.AddAsync(employee);

            return Ok(employeeId);
        }

        /// <summary>
        /// Обновить данные сотрудника.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync([FromBody] EmployeeUpdateRequest request, Guid id)
        {
            var oldEmployee = await _employeeRepository.GetByIdAsync(id);

            var newEmployee = _mapper.Map<EmployeeUpdateRequest, Employee>(request, oldEmployee);

            await _employeeRepository.UpdateAsync(newEmployee);

            return NoContent(); 
        }

        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}