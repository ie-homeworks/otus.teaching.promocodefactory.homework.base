﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<EmployeeCreateRequest, Employee>();
            CreateMap<EmployeeUpdateRequest, Employee>();
            CreateMap<Employee, EmployeeResponse>();
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Role, RoleItemResponse>();
            CreateMap<RoleItemRequest, Role>();
        }
    }
}